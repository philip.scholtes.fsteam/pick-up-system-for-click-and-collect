# Pick-Up System for Click and Collect

## Description and Motivation
The Pick-up system will be able to give the customer the opportunity to select predefined recipes and order the needed products online anywhere they want via an app. As soon as they finish their order, an employee will gather all the needed products and put them together so the customer just have to come to store a pick it up and pay instead of going through the store and spend more time. The prototype will not have a user management. We think this is sufficient to present the idea.

## Must-Haves

End-User App for the Customer and Picker

We have one end-user app for the customer. In this app the customer can choose of a list of recipes and add them to his order. If the customer adds an recipe, all the products of this recipe will be added to his order automatically. After the customer added all the recipes to his order, the customer can finish the order and the order will be visible in the second end-user app with a notification to the pickers. There will also be an open order tab in the end-user app for the customer to show all of his orders and their current status. The second end-user App is for the picker to view all orders from all the customer. The picker can than mark one order as "in progress" to indicate that the picker is gathering all the products from this order. As soon as the picker is finished with the order the picker can mark the order as "finished". After that the customer of the order will be receiving a notification on his app that the customer can pick up his order with a preview of the receipt, which will be created after the picker is finished with the order. Of course all the data-events(e.g. creating an order) will be dealt by database updates and events.

One Product is not available anymore

If one product is not available anymore, there will be two cases. First the product is not available in the moment the recipe is ordered by the customer. In this case we will notify the customer in the app, that there is a shortage of a product and give him the option to choose whether the customer still wants to continue ordering the recipe without this product but all the others, or the customer can cancel the whole recipe, so no product of this recipe will be added to the order. The other case would be that the picker notices, during the gathering of products that there is a shortage of one or more products. In this case the picker will mark in his app the product as out of stock and the customer will get a notification in his app that one order cannot be finished. In his app in the order tab the customer can select this order and can choose between "finish without product" and "remove the recipe from order". Of course all the data-events(e.g. updating an order) will be dealt by database updates and events.

Fill storage

There will be an fill storage event, when products will be delivered, which updates the data in the storage-entity of the database. This event will be available in the picker app, so the employee can confirm the delivery.

Database

In our database we will have a product-entity for all the different products and in combination with this a product-category-entity for the possible supplement function in the May-Haves. We have the recipe-entity, which includes the products and amount which are needed for this recipe. We have an order-entity, which includes all the products and amount of an order of the customer. The storage-entity will have all the available products and their amount stored. The receipt-entity will include a modified order-entity, with all products of an order and their final amount/weight and price.

Estimated Time

The setup of the Database and the back-end should take about a week. We estimate that the Customer App with all the features will take 2 and a half week to implement and the Picker App with the Fill storage event included will take about 2 weeks. After both Apps are finished we will implement the "Product is not available"-feature, which will take about a week and after that we will test the system and if some time is left implement some May-haves. 

## May-Haves

End-user App extension to order single products

The customer will be able to order single products instead of ordering a prefabricated receipt. This would lead to create a browsing feature for the products in the end-user app for the customer.
The packing process for the picker would stay the same.

Unavailable products will be replaced

Unavailable items from an ordered recipe will be replaced if an alternative is available, the customer gets a notification in his end-user app and has to confirm the updated order. We will extend the order tap with a third function "finish order with replacement products".

Remove single products from a recipe

The customer will be able to modify a recipe therefore he can delete single products from a selected recipe. This would help the customer to order only the missing products for a given recipe.

Finding recipes with the help of tags

The customer will be able to browse through the end-user app with the help of different tags, the tags could be something like: vegan, vegetarian, meat, dessert and so on. Therefore we will add a tags-entity to our database. This will lead to a better user experience because the user will be able to find recipes easier and faster.

Customer can select pickup time

The customer will be able to select a pickup time. Therefore we will add a function at the end of the ordering process, in which the customer can choose between different time options. The customer can select between "as soon as possible" or "a specific date". The selected date can only be seven days in advance. We will filter the view of the orders in the picker app, so that the picker knows which order has the highest priority.

## Possible add-ons

End-user App extension with a favorite list

The customer will be able to mark and save his favorite recipes in a "favorite list", that would help the users who always order the same recipes. 

End-user App extensions with a feedback function

We will add a simple feedback function to every single recipe in form of a "thumbs up" or "thumbs down".
This will be clearly visible on the recipe page, with a counter on how many thumbs up or thumbs down a recipe got.

## Must-Not Haves

Creation of own recipes

In the end-user app for the customer, customers will not be able to create their own recipes out of the available products, save them, or make them publicly available for other customers.
To implement that feature changes to the UI would be required and custom recipes would need to be stored. Because of that reasons we decided not to realize this feature.

Online Payment

Another feature that will not be implemented in the end-user app for the customer is the option to directly pay the ordered products via an online payment service. 
We decided not to implement this feature due to the overhead it would take to add a online payment service in a secure way.

Customer Feedback in form of comments

Customers will not be able to review recipes in the end-user app with comments, or see comments from other customers reviewing a specific recipe. 
This feature will not be implemented because it requires changes to the UI and we would need to store additional data. We also need a user management, which we do not plan to implement.

Manager/Statistics UI

There will be no app and UI for supermarket managers or employees to look at some statistics regarding for example the popularity of recipes, the amount of orders per recipe, or to review the storage efficiency.
Since this would require a completely new app and a new UI we decided against that feature.

Estimated Pickup Time

After a successful order the customer will not receive an estimated time at which he can pick up his order.
In order to get a fitting estimated pickup time we would need to have some kind of machine learning algorithm which would be a large overhead or we use something like an average time of previous orders which could lead to inaccuracy.

Multiple Picker Views

Instead of one view for all pickers, where they can see all currently open orders, each picker could get his own view where the pickers can only see orders that have been automatically assigned by the back-end to the specific picker.
We also need a user management, which we do not plan to implement.