using { sap.pickandcollect as my} from '../db/schema';
service CatalogService @(path: '/browse'){

  /** For display in details pages */
  @readonly entity Product as projection on my.Product excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Recipe as projection on my.Recipe excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Ingredient as projection on my.Ingredient excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Product_Category as projection on my.Product_Category excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Storage as projection on my.Storage excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Receipt as projection on my.Receipt excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Order_Item as projection on my.Order_Item excluding { createdBy, modifiedBy, createdAt, modifiedAt };

  @readonly entity Order as projection on my.Order excluding { createdBy, modifiedBy, createdAt, modifiedAt };
  
  action sendMail (MailText: String, Emailadress: String) returns {info: String};

  action submitOrder (recipe: Recipe:ID, amount: Integer) returns { order_id: Integer}
  


}