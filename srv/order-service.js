const cds = require('@sap/cds')

module.exports = function Order() {
	const { Storage, Receipt } = srv.entities
	const tx = cds.transaction(req)

	// Check all amounts against stock before activating
  this.before(['CREATE', 'UPDATE'], 'finishOrder', async (req) => {
    const {ID: orderId} = req.data
		const order = await SELECT.one.from('Order').where({ID:orderId})
		const orderItems = order.products

    return Promise.all(orderItems.map(async (each) => {
			const orderItemId = each
			const orderItem = await SELECT.one.from('OrderItem').where({ID:orderItemId})
			const productId = orderItem.product
			return tx.run(
				UPDATE(Storage)
					.where({ product: productId })
					.and(`stock >=`, orderItem.amount)
					.set(`stock -=`, orderItem.amount)
    )}.then(affectedRows => {
      if (!affectedRows) {
        req.error(409, `${each.amount} exceeds stock for storage product #${each.ID}`)
      }
    })))
  })

	// Generate receipts if products are at stock
	this.on('finishOrder', async (req) => {
		const {ID: orderId} = req.data
		const order = await SELECT.one.from('Order').where({ID:orderId})
		const orderItems = order.products

		let totalPrice = 0

		orderItems.map(async (each) => {
			const orderItemId = each
			const orderItem = await SELECT.one.from('OrderItem').where({ID:orderItemId})
			const productId = orderItem.product
			const product = await SELECT.one.from('Product').where({ID:productId})
			const price = product.price
			totalPrice += price
		})

		return tx.run([
			INSERT.into(Receipt).entries({ price: totalPrice, products})
    ]).catch(() => req.reject(400, 'Something went wrong while creating receipt!'))
	})
}