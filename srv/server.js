const cds = require('@sap/cds');

  module.exports = (o)=>{
    o.app = require('express')()
    cookieParser = require('cookie-parser');
    const UserManagement = require('../middleware/user-management.js');

    // Add cookieParser and UserManagement Middleware
    o.app.use(cookieParser()); 
    o.app.use(UserManagement);
    
    return cds.server(o) //> delegate to default server.js
  }
