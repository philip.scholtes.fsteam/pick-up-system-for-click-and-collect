const express = require('express');
var crypto = require('crypto');
const cds = require('@sap/cds')

module.exports = async function UserManagement(req, res, next) 
{
    /* Check the URL Query for a valid userToken, so we are able
       to set cookies via URLs with domain.org/page?userToken=...*/
    var userToken = req.query['userToken'];
    
    if (userToken == null || !await UserExists(userToken))
    {
        // Check if user already has a userToken in cookies
        userToken = GetUserToken(req);

        if (userToken == null || !await UserExists(userToken))
        {
            // Create a new user and userToken
            userToken = await CreateUser();
        }
    }

    /* Set a cookie "userToken" that hold for 5 years, sameSite set to Strict 
       and using HttpOnly */
    res.cookie("userToken", userToken, {maxAge: 5 * 365 * 24 * 60 * 60 * 1000,
        sameSite: 'Strict', httpOnly: true});

    // Call the next middleware
    next();
}

// Get the userToken from the cookie
function GetUserToken(req)
{
    return req.cookies['userToken'];
}

// Create a new User
async function CreateUser()
{
    // Select the User database
    let {User} = cds.entities ('sap.pickandcollect');

    var token;

    // Create a token and check if it is not already used by another user
    do 
    {
        token = crypto.randomBytes(32).toString('base64'); 
    } while (await UserExists(token))

    // Insert new user in the database
    await INSERT.into(User).entries({Token: token, email: "", name: "", prename: ""});
    return token;
}

// Update email, name and prename of a user
async function UpdateUser(token, email, name, prename)
{
    let {User} = cds.entities ('sap.pickandcollect');
    await UPDATE(User,token).with({email: email, name: name, prename: prename});
}

async function UserExists(token)
{
    // Select the User database
    let {User} = cds.entities ('sap.pickandcollect');
    
    var result = (await SELECT `Token`.from(User, token));
    return result != null && result.length != 0;
}
