using { Currency, managed, sap } from '@sap/cds/common';
namespace sap.pickandcollect;

entity Product: managed {
    key ID   : Integer;
    name     : String(111);
    descr    : String(1111);
    price    : Decimal(9,2);
    isLoose  : Boolean;             //Attribute to determine wether a product is charged by amount or weight. True = weight, False = amount
    Currency : Currency;
    product_category : Association to Product_Category;
}

entity Product_Category : sap.common.CodeList {
    key ID  : Integer;
    parent  : Association to Product_Category;
    children: Composition of many Product_Category on children.parent = $self;
}

//Help-Entity for recipe for every product in recipe
entity Ingredient : managed {
    key ID : Integer;
    product: Association to Product;
    amount : Integer;
    weight : Decimal(9,4);
    recipe : Association to Recipe;
}

entity Recipe : managed {
    key ID   : Integer;
    name     : String(111);
    descr    : String(1111);
    products : Association to many Ingredient on products.recipe = $self;
}

//Maybe merge Storage and Product entity
entity Storage : managed {
    key ID  : Integer;
    product : Association to Product;
    amount  : Integer;
    weight  : Decimal(9,4);
}
/*
//Help-Entity for order and receipt for every product in order and receipt

entity Order_Item : managed {
    key ID  : Integer;
    product : Association to Product;
    amount  : Integer;
    weight  : Decimal(9,4);
    actual_weight : Decimal(9,4);
    order   : Association to Order;
    receipt : Association to Receipt;
}

Included in the Order entity

 */
entity Order : managed {
    key ID   : Integer;
    status   : String(111);
    Items    : Composition of many {
        key ID  : Integer;
        product : Association to Product;
        amount  : Integer;
        weight  : Decimal(9,4);
        actual_weight : Decimal(9,4);
        receipt : Association to Receipt;

    };
    user     : Association to User;
}

//Maybe merge Receipt and Order entity into one
entity Receipt : managed {
    key ID  : Integer;
    price   : Decimal(9,2); 
    products: Association to many Order_Item on products.receipt = $self; 
}

entity User : managed {
    key Token : String(111);
    email : String(111);
    name : String(111);
    prename : String(111);
}